import ntnu.idatt1002.Factory;
import ntnu.idatt1002.PopUp;
import ntnu.idatt1002.Popups.Add;
import ntnu.idatt1002.Popups.Help;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class FactoryTest {

    @Test
    public void InvalidPopUpTextTest(){
        PopUp popUp1 = Factory.getPopup("Invalid Text");
        assertNull(popUp1);
    }

    @Test
    public void createPopUp(){
        assertTrue(Factory.getPopup("help") instanceof Help);
    }



}
