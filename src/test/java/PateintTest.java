import ntnu.idatt1002.Patient;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class PateintTest {
    Patient p1 = new Patient("12312312311", "Wilson", "Volly", "too Round", "coconuts");


    @Test
    public void patientGettersAndNumberValidation(){
        assertEquals(p1.getSocialSecurityNumber(),"12312312311");
        assertEquals(p1.getFirstName(),"Wilson");
        assertEquals(p1.getLastName(),"Volly");
        assertEquals(p1.getDiagnosis(),"too Round");
        assertEquals(p1.getGenrealPractitioner(),"coconuts");
        assertThrows(new IllegalArgumentException().getClass(),() -> {p1.setSocialSecurityNumber("1");});
    }


    }
