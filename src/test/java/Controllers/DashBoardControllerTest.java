package Controllers;

import ntnu.idatt1002.Controllers.DashBoardController;
import org.junit.jupiter.api.Test;

import java.io.File;

import static org.junit.jupiter.api.Assertions.*;

public class DashBoardControllerTest {
    private File test1 = new File("src/main/resources/test.csv");
    private File test2 = new File("src/main/resources/test.asdsdassdasda");

    @Test
    public void importFileWorksTest(){
        assertTrue(DashBoardController.validateFileFormat(test1));
    }

    @Test
    public void importFileWithWrongNameTest(){
        assertFalse(DashBoardController.validateFileFormat(test2));
    }

    @Test
    public void exportFileIfTheFileAlreadyExistsTest(){
        assertFalse(DashBoardController.alreadyExists(test1.toString()));
        assertTrue(DashBoardController.alreadyExists("src/main/resources/Icons/add.png"));
    }
}
