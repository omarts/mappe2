module ntnu.idatt1002 {
    requires javafx.controls;
    requires javafx.fxml;
    requires java.desktop;
    requires commons.csv;

    exports ntnu.idatt1002;
    opens ntnu.idatt1002.Controllers to javafx.fxml;
}