package ntnu.idatt1002;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

public class App extends Application {
    public static Stage stage;
    /**
     * A method to start the program
     * @param stage
     * @throws IOException
     */
    @Override
    public void start(Stage stage) throws IOException {
        Scene scene = new Scene(loadFXML("DashBoard"));
        stage.setScene(scene);
        stage.setTitle("PatientsRegister");
        stage.show();
    }


    /**
     * Main method
     * @param args
     */
    public static void main(String[] args) {
        launch();
    }


    /**
     * Method used for loading fxml file
     * @param fxml the name of the fxml file
     * @return
     * @throws IOException
     */
    public static Parent loadFXML(String fxml) throws IOException {
            FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("/fxml/" + fxml + ".fxml"));
            return fxmlLoader.load();
    }

    public static Stage getStage(){
        return stage;
    }
}
