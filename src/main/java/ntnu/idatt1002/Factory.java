package ntnu.idatt1002;

import ntnu.idatt1002.Popups.*;
import ntnu.idatt1002.Popups.Error;

/**
 * The Factory class is used to call upon different PopUp objects.
 * Is used in controller.
 */
public class Factory {
    /**
     * Methode to retreve different PopUps depending on the required use.
     * @param popUpType String that corresponds to a PopUp
     * @return a PopUp
     */
    public static PopUp getPopup(String popUpType){
        switch (popUpType){
            case "add":
                return new Add();
            case "delete":
                return new Delete();
            case "edit":
                return new Edit();
            case "error":
                return new Error();
            case "help":
                return new Help();
            case "replace":
                return new ReplaceFileConfirmation();
        }
        return null;
    }
}
