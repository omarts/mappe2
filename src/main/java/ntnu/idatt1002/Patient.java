package ntnu.idatt1002;

import javafx.beans.property.SimpleStringProperty;

import java.io.Serializable;

/**
 * The Patient class creates the object Patient. This object is Serializable.
 * The patient is used to represent a Patient at a hospital.
 * The variables of the patient reflects this, as it is the information you would expect to see of a patient at a hospital.
 */
public class Patient implements Serializable {

    SimpleStringProperty socialSecurityNumber;
    SimpleStringProperty firstName;
    SimpleStringProperty lastName;
    String diagnosis;
    String genrealPractitioner;

    /**
     * Basic constructor. the constructor takes inn 5 strings as parameters. The first string is checked if it has lenght 11.
     * If the string does not have lenght 11, the constructor will throw a illegal argument exception, witch information about the lenght being wrong.
     * @param socialSecurityNumber
     * @param firstName
     * @param lastName
     * @param diagnosis
     * @param genrealPractitioner
     */
    public Patient(String socialSecurityNumber, String firstName, String lastName, String diagnosis, String genrealPractitioner) {
        if(socialSecurityNumber.length()!=11){
            throw new IllegalArgumentException("The Social Security Number should be 11 digits.");
        } else {
            this.socialSecurityNumber = new SimpleStringProperty(socialSecurityNumber);
            this.firstName = new SimpleStringProperty(firstName);
            this.lastName = new SimpleStringProperty(lastName);
            this.diagnosis = diagnosis;
            this.genrealPractitioner = genrealPractitioner;
        }
    }

    public String getSocialSecurityNumber() {
        return socialSecurityNumber.get();
    }

    public SimpleStringProperty socialSecurityNumberProperty() {
        return socialSecurityNumber;
    }

    public void setSocialSecurityNumber(String socialSecurityNumber)throws IllegalArgumentException {
        if(socialSecurityNumber.length()!=11){
            throw new IllegalArgumentException();
        }else {
            this.socialSecurityNumber.set(socialSecurityNumber);
        }
    }

    public String getFirstName() {
        return firstName.get();
    }

    public SimpleStringProperty firstNameProperty() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName.set(firstName);
    }

    public String getLastName() {
        return lastName.get();
    }

    public SimpleStringProperty lastNameProperty() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName.set(lastName);
    }

    public String getDiagnosis() {
        return diagnosis;
    }

    public void setDiagnosis(String diagnosis) {
        this.diagnosis = diagnosis;
    }

    public String getGenrealPractitioner() {
        return genrealPractitioner;
    }

    public void setGenrealPractitioner(String genrealPractitioner) {
        this.genrealPractitioner = genrealPractitioner;
    }

    /**
     * To String methode that display the information of the patient in a organized string.
     * @return a string that descripes the patient.
     */
    @Override
    public String toString() {
        return "Patient{" +
                "socialSecurityNumber='" + socialSecurityNumber + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", diagnosis='" + diagnosis + '\'' +
                ", genrealPractitioner='" + genrealPractitioner + '\'' +
                '}';
    }
}
