package ntnu.idatt1002.Controllers;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.stage.FileChooser;
import ntnu.idatt1002.*;
import ntnu.idatt1002.Popups.*;
import ntnu.idatt1002.Popups.Error;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import javax.swing.*;
import javax.swing.filechooser.FileSystemView;
import java.io.*;

import java.util.Optional;

/**
 * The DashBoardController class is a controller class that interacts with the DashBoard.fxml. This controller updates the TableView with the stored patients.
 * the Patients are Stored in a ObservableList<Patient>. The controller handles all button presses that happens on the fxml.
 * It also handles logic conserning importing and exporting the Observable list into csv files.
 *
 * When different buttons are pressed in the fxml, the controller often calls upon a fitting PopUp depending on the action.
 */
public class DashBoardController {

    @FXML private TableView<Patient> tablePatient;
    @FXML private TableColumn<Patient,String> firstNameCol;
    @FXML private TableColumn<Patient,String> lastNameCol;
    @FXML private TableColumn<Patient,String> ssnCol;
    @FXML private Label statusLabel;
    @FXML private Button addButton;
    @FXML private Button deleteButton;
    @FXML private Button editButton;
    public static ObservableList<Patient> Patients = FXCollections.observableArrayList();

    /**
     * The initialize methode is used to activate the table, and is used to set a Label text to a basic start string.
     */
    public void initialize() {
        setStatusLabel("Start");
        setTable();
    }

    /**
     * Sets icons for buttons, and formats the TableView. also specifing the object which is displayed.
     */
    public void setTable(){
        addButton.setGraphic(new ImageView("./icons/add.png"));
        deleteButton.setGraphic(new ImageView("./icons/delete.png"));
        editButton.setGraphic(new ImageView("./icons/edit.png"));
        tablePatient.setEditable(true);
        firstNameCol.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        lastNameCol.setCellValueFactory(new PropertyValueFactory<>("lastName"));
        ssnCol.setCellValueFactory(new PropertyValueFactory<>("socialSecurityNumber"));
        tablePatient.setItems(Patients);
    }

    /**
     * Methode for the "add" button. The add methode calls upon a PopUp of class Add.
     * And set the statusLabel to reflect the addition action completion.
     */
    public void add() {
        PopUp popup = Factory.getPopup("add");
        Add add = (Add) popup;
        add.getPop();
        add.getDialog().showAndWait();
        setStatusLabel("Addition Complete");
    }

    /**
     * Methode for the "Delete" button. The add methode calls upon a PopUp of class Delete.
     * It checks if a item has been selected from the tableView.
     * And set the statusLabel to reflect the Deletion action completion.
     */
    public void delete(){
        if(!tablePatient.getSelectionModel().isEmpty()) {
            Patient p = tablePatient.getSelectionModel().getSelectedItem();
            PopUp popup = Factory.getPopup("delete");
            Delete delete = (Delete) popup;
            delete.getPop();
            delete.deletePatient(p);
            setStatusLabel("Deletion Complete");
        }
    }

    /**
     * Methode for the "Edit" button. The add methode calls upon a PopUp of class Edit.
     * It checks if a item has been selected from the tableView.
     * And set the statusLabel to reflect the Editing action completion.
     */
    public void edit(){
        if(!tablePatient.getSelectionModel().isEmpty()) {
            Patient p = tablePatient.getSelectionModel().getSelectedItem();
            PopUp popup = Factory.getPopup("edit");
            Edit edit = (Edit) popup;
            edit.getPop();
            edit.setNewPatient(p);
            setStatusLabel("Editing Complete");
        }

    }

    /**
     * Methode that is used to write a csv file.
     * The methode writes the variables of all patient in the ObservableArray Patients into a csv file.
     * after exporting the patients, the StatusLabel text is also altered to reflect the completion of this action.
     * @param filename name of file that is being created.
     * @throws IOException
     */
    public void writeFile(String filename) throws IOException {
        FileWriter fileWriter = new FileWriter(filename);
        CSVPrinter printer = new CSVPrinter(fileWriter, CSVFormat.DEFAULT);
        printer.printRecord("First Name; Last name; Genreal practinioner; SocialSecurityNumber");
        if (Patients != null) {
            for (Patient patient : Patients) {
                printer.printRecord(
                        patient.getFirstName(),
                        patient.getLastName(),
                        patient.getGenrealPractitioner(),
                        patient.getSocialSecurityNumber()
                );
            }
            printer.flush();
            printer.close();
            fileWriter.close();
            setStatusLabel("The export is complete");
        }
    }

    /**
     * Checks if a file already is a file. if it is, then it returns true.
     * @param filename name of file
     * @return
     */
    public static boolean alreadyExists(String filename){
        if(new File(filename).isFile()){
            return true;
        }
        return false;
    }
    /**
     * Methode is called on when the export button is selected in the fxml file.
     * Methode that lets the user specify a filename, and uses the writeFile methode to write a file.
     * This methode provides a GUI for the user, letting them select a filename, or replace a previouse file.
     * The methode calls upon different Popup as a reaction to what the user does.
     * The GUI differs depending on if the user wants to replace a old file, or just make a file.
     * @throws IOException
     */
    public void makeFileGUI() throws IOException {
        //let user make file path
        JFileChooser j = new JFileChooser(FileSystemView.getFileSystemView().getDefaultDirectory());
        j.showSaveDialog(null);

        String filename = j.getSelectedFile().getAbsolutePath();
        if(alreadyExists(filename)){

            PopUp popup = Factory.getPopup("replace");
            ReplaceFileConfirmation replace = (ReplaceFileConfirmation) popup;
            replace.getPop();
            Optional<ButtonType> result = replace.getAlert().showAndWait();

            if(result.get()==ButtonType.OK){
                writeFile(filename);
            }
        }else {
            writeFile(filename);
        }
    }

    /**
     * The methode is called upon when the import button is used on the fxml file.
     * The methode lets the user select a file from their fileExplorer. This file is then checked if it is a csv file.
     * If the file is not a csv file, a PopUp of class Error is called upon, to give the user a error message with the nesecerry information.
     * If the right filetype is selected, patients will be constructed from the information in the file, and be set in the tableView.
     * Lastly the StatusLabel text is set to reflect the completion of the import.
     */
    public void importFile(){
        File file = new FileChooser().showOpenDialog(App.getStage());
        boolean validate = validateFileFormat(file);
        if (validate){
            setStatusLabel("Import sucsesful: The patients are imported");
            try {
                BufferedReader br = new BufferedReader(new FileReader(file.getPath()));
                br.readLine();
                String line1 = null;
                while ((line1 = br.readLine()) != null) {
                    String[] values = line1.split(";");
                    Patients.add(new Patient(values[3], values[0], values[1], null, values[2]));
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }else{
            PopUp popup = Factory.getPopup("error");
            Error error = (Error) popup;
            error.getPop();
            error.getAlert().setContentText("Wrong File Type");
            error.getAlert().showAndWait();
        }
    }

    /**
     *  Methode to validate if a file is of the right format.
     * @param file file that is being checked.
     * @return
     */
    public static boolean validateFileFormat(File file) {
        if (!file.getName().substring(file.getName().indexOf('.')).equals(".csv")) {
            return false;
        }
        return true;
    }

    /**
     * Methode to change the text of the StatusLabel at the button of the FXML file.
     * @param status String that describes the last action completed by the program.
     */
    public void setStatusLabel(String status){
        statusLabel.setText("Status: " + status);
    }

    /**
     * Methode that is called upon when the help button is used. It calls upon a PopUp of class Help.
     */
    public void help(){
        PopUp popup = Factory.getPopup("help");
        Help help = (Help) popup;
        help.getPop();
    }

}
