package ntnu.idatt1002.Popups;

import javafx.scene.control.Alert;
import ntnu.idatt1002.PopUp;

/**
 * The class Error is a class that implements the PopUp class.
 * it provides a alert of type Error.
 */
public class Error implements PopUp {
    Alert alert = new Alert(Alert.AlertType.ERROR);

    /**
     * setting the main text of the alert.
     */
    @Override
    public void getPop() {
        alert.setTitle("Error");
        alert.setContentText("You've got an Error Mister");
    }

    /**
     * getter methode for the alert.
     * @return the alert.
     */
    public Alert getAlert() {
        return alert;
    }
}
