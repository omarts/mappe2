package ntnu.idatt1002.Popups;

import javafx.scene.control.Alert;
import ntnu.idatt1002.PopUp;

/**
 * The Help class implements the PopUp class.
 * The methode provides a alert of type information.
 * The class sets some text in the alert, and displayes it.
 */
public class Help implements PopUp {
    @Override
    public void getPop(){
        Alert help = new Alert (Alert.AlertType.INFORMATION);
        help.setTitle("Hello there");
        help.setHeaderText(null);
        help.setContentText("General Kenobi");
        help.showAndWait();
    }
}
