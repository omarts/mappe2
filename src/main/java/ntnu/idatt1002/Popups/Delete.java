package ntnu.idatt1002.Popups;

import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import ntnu.idatt1002.Controllers.DashBoardController;
import ntnu.idatt1002.Patient;
import ntnu.idatt1002.PopUp;

import java.util.Optional;
/**
 * The Delete class, is a class that implements the PopUp interface.
 * The class creates a Confirmation alert.
 * When the confirm button is hit, this will also collapse the dialog box.
 * If the cancel box is used, the dialog will collapse without deleting any patient.
 *
 * The class provides a methode to delete a given patient from the Observable array in the controller class.
 */
public class Delete implements PopUp {
    Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
    @Override
    public void getPop() {

        alert.setTitle("Confirm delete");
        alert.setContentText("Are you sure you want to delete this patient?");

    }

    /**
     * Methode that takes a patient and delete is if the confirm button is used inside the alert.
     * @param p Takes a patient as parameter.
     */
    public void deletePatient(Patient p){
        Optional<ButtonType> result = alert.showAndWait();
        if(result.get()==ButtonType.OK){
            DashBoardController.Patients.remove(p);
        }
    }
}
