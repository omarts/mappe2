package ntnu.idatt1002.Popups;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import ntnu.idatt1002.Controllers.DashBoardController;
import ntnu.idatt1002.Patient;
import ntnu.idatt1002.PopUp;


/**
 * The Edit class, is a class that implements the PopUp interface.
 * The class creates a dialog which is filled with textFields, and two buttons.
 * The data that is put in the textFields will be converted into a Patient, and added to the observable array in the controller class
 * when the confirm button is hit, it will also delete the original Patient.
 * if the cancel box is used, the dialog will collapse without making or deleting any patient.
 */
public class Edit implements PopUp {
    Dialog<String> dialog = new Dialog<>();
    GridPane grid = new GridPane();
    Button confirmButton = new Button("Confirm");
    @Override
    public void getPop() {

        dialog.setTitle("Edit Patient");
        dialog.getDialogPane().getButtonTypes().addAll(ButtonType.CANCEL);



    }

    /**
     * Methode that lets the user edit a patient. This is done by providing the original information of a Patient to the user,
     * and letting them edit the names and Social security number.
     * The methode creates a new patient with the new information, and deletes the old one.
     * @param p the patient that is being edited.
     */
    public void setNewPatient(Patient p){
        TextField firstName = new TextField();
        firstName.setPromptText("First Name");
        firstName.setText(p.getFirstName());

        TextField lastName = new TextField();
        lastName.setPromptText("Last Name");
        lastName.setText(p.getLastName());

        TextField ssn = new TextField();
        ssn.setPromptText("Social Security Number");
        ssn.setText(p.getSocialSecurityNumber());

        grid.setHgap(5);
        grid.setVgap(5);
        grid.setPadding(new Insets(100,10,10,10));


        grid.add(confirmButton,3,3);
        grid.add(new Label("First Name"),0,0);
        grid.add(firstName,1,0);
        grid.add(new Label("Last Name"),0,1);
        grid.add(lastName,1,1);
        grid.add(new Label("Social Security Number"),0,2);
        grid.add(ssn,1,2);

        dialog.getDialogPane().setContent(grid);

        //Adding a new patient, and removing the old one from the TableView.
        confirmButton.setOnAction(actionEvent -> {
            DashBoardController.Patients.add(new Patient(ssn.getText(),firstName.getText(),lastName.getText(),null,null));
            DashBoardController.Patients.remove(p);
            dialog.close();
        });
        dialog.showAndWait();
    }
}
