package ntnu.idatt1002.Popups;

import javafx.geometry.Insets;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;
import ntnu.idatt1002.Controllers.DashBoardController;
import ntnu.idatt1002.Patient;
import ntnu.idatt1002.PopUp;

/**
 * The Add class, is a class that implements the PopUp interface.
 * The class creates a dialog which is filled with textFields, and two buttons.
 * The data that is put in the textFields will be converted into a patient, and added to the observable array in the controller class
 * when the confirm button is hit, this will also collapse the dialog box.
 * if the cancel box is used, the dialog will collapse without making any patient.
 */
public class Add implements PopUp {
    Dialog<String> dialog = new Dialog<>();
    @Override
    public void getPop() {
        //Making the dialog, and adding the needed componets.
        dialog.setTitle("Add Patient");
        Button confirmButton = new Button("Confirm");
        dialog.getDialogPane().getButtonTypes().addAll(ButtonType.CANCEL);

        GridPane grid = new GridPane();
        grid.setHgap(5);
        grid.setVgap(5);
        grid.setPadding(new Insets(100,10,10,10));

        TextField firstName = new TextField();
        firstName.setPromptText("First Name");
        TextField lastName = new TextField();
        lastName.setPromptText("Last Name");
        TextField ssn = new TextField();
        ssn.setPromptText("Social Security Number");

        grid.add(confirmButton,3,3);
        grid.add(new Label("First Name"),0,0);
        grid.add(firstName,1,0);
        grid.add(new Label("Last Name"),0,1);
        grid.add(lastName,1,1);
        grid.add(new Label("Social Security Number"),0,2);
        grid.add(ssn,1,2);

        dialog.getDialogPane().setContent(grid);

        // Confirm button press
        confirmButton.setOnAction(actionEvent -> {
            DashBoardController.Patients.add(new Patient(ssn.getText(), firstName.getText(), lastName.getText(), null, null));
            dialog.close();
        });
    }

    public Dialog<String> getDialog() {
        return dialog;
    }
}
