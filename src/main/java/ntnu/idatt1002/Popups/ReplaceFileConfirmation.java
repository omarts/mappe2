package ntnu.idatt1002.Popups;

import javafx.scene.control.Alert;
import ntnu.idatt1002.PopUp;

/**
 * The ReplaceFileConfirmation class implements PopUp class.
 * The class creates a Alert of type Confirmation.
 * The methode sets some text of the alert and lets it be manipulated with a getter methode.
 */
public class ReplaceFileConfirmation implements PopUp {
    Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
    @Override
    public void getPop(){

        alert.setTitle("There already exists a file with that name");
        alert.setContentText("Do you want to replace it?");
        alert.showAndWait();
    }

    /**
     * getter methode
     * @return the alert
     */
    public Alert getAlert(){
       return alert;
    }
}
