package ntnu.idatt1002;

/**
 * Interface class that provides a void methode.
 */
public interface PopUp {
    void getPop();
}
